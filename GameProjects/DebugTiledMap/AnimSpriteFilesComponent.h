// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include "SpriteComponent.h"
#include <unordered_map>

class Actor;
struct TextureProp;
class AnimSpriteFilesComponent : public SpriteComponent
{
public:
	AnimSpriteFilesComponent( Actor* owner, int drawOrder = 100);
	// Update animation every frame (overriden from component)
	void Update(float deltaTime) override;
	// Set the textures used for animation
	void SetAnimTexturesFiles(const std::vector<TextureProp*>& textures);
	// Set/get the animation FPS
	float GetAnimFPS() const { return mAnimFPS; }
	void SetCurAnim(std::string animName);
	void SetAnimFPS(float newFPS) { mAnimFPS = newFPS; }
	void SetAnim(std::string nameofAnim, int animStartInText, int animEndInText, bool looping);
	~AnimSpriteFilesComponent();

	AnimSpriteFilesComponent* GetSpriteComp() { return this; }
private:
	// All textures in the animation
	std::vector<TextureProp*> mAnimTextures;
	// All Files used in animations
	std::vector <std::string*> mAnimTextureNames;
	// Animation frame rate
	float mAnimFPS;
	//vector of name of animation and its texture range
	std::unordered_map<std::string, struct AnimwithFrames*> mAnims;
	//current frame displayed
	float mCurrFrame;
	struct AnimwithFrames* mCurAnim;
};
