#pragma once
#include "SpriteComponent.h"
#include <string>
#include <unordered_map>

class Actor;
class string;
struct TextureProp;
class AnimSpriteSheetComponent : public SpriteComponent
{
	public:
		AnimSpriteSheetComponent(Actor* owner, int drawOrder = 100);
		// Update animation every frame (overriden from component)
		void Update(float deltaTime) override;
		// Set/get the animation FPS
		float GetAnimFPS() const { return mAnimFPS; }
		void SetAnimFPS(float newFPS) { mAnimFPS = newFPS; }
		void SetCurAnim(std::string animName, float fPS);
		void SetAnim(std::string nameofAnim, bool doesLoop, float frameWidth, float frameHeight, std::vector<std::tuple<int, int>>& animFramePos);
		void SetAnim(std::string nameofAnim, bool doesLoop, float frameWidth, float frameHeight, int frameCount);
		~AnimSpriteSheetComponent();
		AnimSpriteSheetComponent* GetAnimSpriteSheetComp() { return this; }
	private:
		// Animation frame rate
		float mAnimFPS;
		//vector of name of animation and its texture range
		std::unordered_map<std::string, struct AnimData*> mAnims;
		//current frame displayed
		float mCurrFrame;
		struct AnimData* mCurAnim;
};

