#include "BGSpriteComponent.h"
#include "Actor.h"
#include "raymathMine.h"

BGSpriteComponent::BGSpriteComponent(class Actor* owner, int drawOrder)
	:SpriteComponent(owner, drawOrder)
	, mScrollSpeed(0.0f)
{
	mRectOrigin = new RayMath::Vector2();
	mRectOrigin->x = 0;
	mRectOrigin->y = 0;
}

void BGSpriteComponent::Update(float deltaTime)
{
	SpriteComponent::Update(deltaTime);
	for (auto& bg : mBGTextures)
	{
		if (mScrollSpeed != 0)
		{
			// Update the x offset
			bg.mOffset.x += mScrollSpeed * deltaTime;
			// If this is completely off the screen, reset offset to
			// the right of the last bg texture
			if (bg.mOffset.x < -mScreenSize.x)
			{
				bg.mOffset.x = (mBGTextures.size() - 1) * mScreenSize.x - 1;
			}
		}
	}
}

void BGSpriteComponent::Draw()
{
	// Draw each background texture
	for (auto& bg : mBGTextures)
	{
		//mTexture = bg.mTexture;
		//SpriteComponent::Draw();
		Rectangle mSourceRec;
		Rectangle mDestRec;
		mSourceRec = { mRectOrigin->x, mRectOrigin->y, (float)bg.mTexture->width, (float)bg.mTexture->height };
		// Assume screen size dimensions
		//mDestRec.width = static_cast<int>(mScreenSize.x);
		//mDestRec.height = static_cast<int>(mScreenSize.y);
		mDestRec.width = static_cast<int>(bg.mTexture->width * mOwner->GetScale().x);
		mDestRec.height = static_cast<int>(bg.mTexture->height * mOwner->GetScale().y);
		// Center the rectangle around the position of the owner
		mDestRec.x = static_cast<int>(mOwner->GetPosition().x);
		mDestRec.y = static_cast<int>(mOwner->GetPosition().y);
		// Center the rectangle around the position of the 
		//mDestRec.x = static_cast<int>(mScreenSize.x / 2) + bg.mOffset.x;
		//mDestRec.y = static_cast<int>(mScreenSize.y / 2) + bg.mOffset.y;

		// Draw this background
		Vector2 origin;
		//origin.Set( (float)mTexture->width, (float)mTexture->height);
		//origin.Set((float)mSourceRec.width / 2, (float)mSourceRec.height / 2);

		Vector2 originRV = { mDestRec.width / 2.0f , mDestRec.height /2.0f };
		// Draw (have to convert angle from radians to degrees)
		 // NOTE: Using DrawTexturePro() we can easily rotate and scale the part of the texture we draw
		// sourceRec defines the part of the texture we use for drawing
		// destRec defines the rectangle where our texture part will fit (scaling it to fit)
		// origin defines the point of the texture used as reference for rotation and scaling
		// rotation defines the texture rotation (using origin as rotation point)
		DrawTexturePro((bg.mTexture->texture), mSourceRec, mDestRec, originRV, mOwner->GetRotation() / DEG2RAD, WHITE);
	}
}

void BGSpriteComponent::SetBGTextures(const std::vector<TextureProp*>& textures)
{
	int offsetTotal = 0;
	int count = 0;
	for (auto tex : textures)
	{
		BGTexture temp;
		temp.mTexture = tex;
		// Each texture is screen width in offset
		temp.mOffset.x = count * mScreenSize.x;
		temp.mOffset.y = 0;
		mBGTextures.emplace_back(temp);
		count++;
	}
}