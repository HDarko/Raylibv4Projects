#include "CircleComponent.h"
#include "Actor.h"
#include "SanjayMath.h"

CircleComponent::CircleComponent(class Actor* owner)
	:Component(owner)
	, mRadius(0.0f)
{

}

const Math::Vector2& CircleComponent::GetCenter() const
{
	return mOwner->GetPosition();
}

float CircleComponent::GetRadius( bool useScaleWidth) const
{
	if (useScaleWidth)
	{
		return mOwner->GetScale().x * mRadius;
	}
	else 
	{
		return mOwner->GetScale().y * mRadius;
	}
	
}

void CircleComponent::DrawDebugCircle(Color color, bool filled)
{
	if (filled)
	{
		DrawCircle(mOwner->GetPosition().x, mOwner->GetPosition().y, mRadius, color);
	}
	else
	{
		DrawCircleLines(mOwner->GetPosition().x, mOwner->GetPosition().y, mRadius, color);
	}
}

bool Intersect(const CircleComponent& a, const CircleComponent& b)
{
	// Calculate distance squared
	Math::Vector2 diff = a.GetCenter() - b.GetCenter();
	float distSq = diff.LengthSq();

	// Calculate sum of radii squared
	float radiiSq = a.GetRadius() + b.GetRadius();
	radiiSq *= radiiSq;

	return distSq <= radiiSq;
}