#include "Game.h"
#include "Actor.h"
#include "raylib.h"
#include "SpriteComponent.h"
#include "BGSpriteComponent.h"
#include "Skeleton.h"
#include "Scarfy.h"
#include "Hazard.h"
#include "TextComponent.h"
#include "SanjayMath.h"
using namespace std;


Game::Game(int screenWidth, int screenHeight, char* gameTitle, int FPS)
	:screenWidth{ screenWidth }
	, screenHeight{ screenHeight }
	, gameTitle{ gameTitle }
	, mIsRunning{true}
	, mPhysicsClock{0.0}
	, mUpdatingActors(false)
	, mFPS(FPS)
	, textActorComp(nullptr)
{
	//Set Debug Log level to record all log errors
	SetTraceLogLevel(LOG_ALL);

	if (FPS > 0)
	{
		SetTargetFPS(FPS);
		mMaxDeltaTime = 0.05;
		
	}
	else
	{
		mMaxDeltaTime = 0.05;
	}
};

bool Game::Initialize()
{
	SetConfigFlags(FLAG_VSYNC_HINT);
	InitWindow(screenWidth, screenHeight, gameTitle);
	LoadData();
	return true;
};

void Game::CheckGameState()
{
	Scarfy* scarf = nullptr;
	Hazard* haz = nullptr;
	for (auto actor : mActors)
	{
		if (scarf==nullptr)
		{
			scarf = dynamic_cast<Scarfy*>(actor);
		}
		if (haz == nullptr)
		{
			haz = dynamic_cast<Hazard*>(actor);
		}
	}
	/*if (scarf && (haz == nullptr))
	{
		textActorComp->SetText("Game Won...Booyaka!", 30, DARKBLUE, screenWidth / 4, screenHeight / 2);
	}
	else if ((scarf == nullptr) && (haz == nullptr))
	{
		textActorComp->SetText("Game Lost...Boo!!...yaka", 30, DARKBLUE, screenWidth / 4, screenHeight / 2);
	}*/
}

void Game::ProcessInput()
{
	//First check if game should be closed
	if (WindowShouldClose())
	{
		mIsRunning = false;
		return;
	}
	//mKeyPressed = GetKeyPressed();

	mUpdatingActors = true;
	for (auto actor : mActors)
	{
		actor->ProcessInput();
	}
	mUpdatingActors = false;

};

void Game::RunLoop()
{
	while (mIsRunning)
	{
		ProcessInput();
		UpdateGame();
		GenerateOutput();
	}
};

void Game::ShutDown()
{
	gameTitle = nullptr;
	UnloadData();

	// Close window and OpenGL context
	CloseWindow();
};

void Game::UnloadData()
{
	// Delete actors
	// Because ~Actor calls RemoveActor, have to use a different style loop
	while (!mActors.empty())
	{
		delete mActors.back();
	}

	for (auto i : mTextures)
	{
		UnloadTexture(*(i.second));
	}
	mTextures.clear();	
}

void Game::UpdateGame()
{
	/* If mFPS is > 0 then its a fixed rate and update as such
	 * else if mFPS is negative then unlocked framerate
	*/
	float deltaTime = GetFrameTime();
	if (deltaTime > mMaxDeltaTime) {
		deltaTime = mMaxDeltaTime;
	}
	//Leave code in. Will implement in box2d if physics required
	/* Fixed delta time value for any physics simulation
		plus the ability to render at different framerates.
	 */
	/*Ensure delta time is never larger than the maximum deltaTime value.
	 *For example due to debugging code with a breakpoint
	 */
	// Update all actors
	mUpdatingActors = true;
	for (auto actor : mActors)
	{
		actor->Update(deltaTime);
	}
	mUpdatingActors = false;

	// Move any pending actors to mActors
	for (auto pending : mPendingActors)
	{
		mActors.emplace_back(pending);
	}
	mPendingActors.clear();

	// Add any dead actors to a temp vector
	std::vector<Actor*> deadActors;

	for (auto actor : mActors)
	{
		if (actor->GetState() == Actor::State::EDead)
		{
			deadActors.emplace_back(actor);
		}
	}

	// Delete dead actors (which removes them from mActors)
	for (auto actor : deadActors)
	{
		delete actor;
	}
	//Update Physics Simulations
	mPhysicsTimeAccumulator += deltaTime;
	while (mPhysicsTimeAccumulator >= mPhysicsTimeStep)
	{
		//previousState = currentState;
		//integrate(currentState, t, dt); or update physics world with time passed and time step
		mPhysicsClock += mPhysicsTimeStep;
		mPhysicsTimeAccumulator -= deltaTime;
	}
	//Interpolate between the reamining time in accumulator ie half a physics step or quarter etc
	//Between current and previous Physics World States
	mPhysicsInterpolationFactor = mPhysicsTimeAccumulator / mPhysicsTimeStep;

		//State state = currentState * alpha +
		//previousState * (1.0 - alpha);
		//Update Physics before Generating Output(Drawing)
		//render(state);
	CheckGameState();
};

void Game::GenerateOutput()
{
	BeginDrawing();
	ClearBackground(RAYWHITE);
	// Draw all sprite components
	for (auto sprite : mSprites)
	{
		sprite->Draw();
		//sprite->DebugDraw();
	}
	DrawFPS(10, 20);
	//DrawRectangle((float)screenWidth / 2, yPos, spriteWidth, spriteHeight, DARKGREEN);
	//DrawRectangle(screenWidth / 2, screenHeight / 2, spriteWidth, spriteHeight, DARKGREEN);
	//string deltaTime = to_string(mDeltaTime);
	//DrawText(deltaTime.c_str(),10, 60,40,RED);
	EndDrawing();

};

void Game::AddActor(Actor* actor)
{
	// If we're updating actors, need to add to pending
	if (mUpdatingActors)
	{
		mPendingActors.emplace_back(actor);
	}
	else
	{
		mActors.emplace_back(actor);
	}
}

void Game::RemoveActor(Actor* actor)
{
	// Is it in pending actors?
	auto iter = std::find(mPendingActors.begin(), mPendingActors.end(), actor);
	if (iter != mPendingActors.end())
	{
		// Swap to end of vector and pop off (avoid erase copies)
		std::iter_swap(iter, mPendingActors.end() - 1);
		mPendingActors.pop_back();
	}

	// Is it in actors?
	iter = std::find(mActors.begin(), mActors.end(), actor);
	if (iter != mActors.end())
	{
		// Swap to end of vector and pop off (avoid erase copies)
		std::iter_swap(iter, mActors.end() - 1);
		mActors.pop_back();
	}
}

void Game::AddSprite(SpriteComponent* sprite)
{
	// Find the insertion point in the sorted vector
	// (The first element with a higher draw order than me)
	int myDrawOrder = sprite->GetDrawOrder();
	auto iter = mSprites.begin();
	for (;
		iter != mSprites.end();
		++iter)
	{
		if (myDrawOrder < (*iter)->GetDrawOrder())
		{
			break;
		}
	}

	// Inserts element before position of iterator
	mSprites.insert(iter, sprite);
}

void Game::RemoveSprite(SpriteComponent* sprite)
{
	// (We can't swap because it ruins ordering)
	auto iter = std::find(mSprites.begin(), mSprites.end(), sprite);
	mSprites.erase(iter);
}

TextureProp* Game::GetTexture(const std::string& fileName)
{
	TextureProp* texture = new TextureProp;
	// Is the texture already in the map?
	auto iter = mTextures.find(fileName);
	if (iter != mTextures.end())
	{
		texture->texture = *(iter->second);
		texture->width = texture->texture.width;
		texture->height = texture->texture.height;
	}
	else
	{
		// Load from file
		texture->texture = (LoadTexture(fileName.c_str()));
		mTextures.emplace(fileName.c_str(), &(texture->texture));
		texture->width = texture->texture.width;
		texture->height = texture->texture.height;
	}
	return texture;
}

bool Game::IsTextureLoaded(const std::string& fileName)
{
	// Is the texture already in the map?
	auto iter = mTextures.find(fileName);
	if (iter != mTextures.end())
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Game::AddHazard(Hazard* haz)
{
	mHazards.emplace_back(haz);
}

void Game::RemoveHazard(Hazard* haz)
{
	auto iter = std::find(mHazards.begin(),
		mHazards.end(),haz);
	if (iter != mHazards.end())
	{
		mHazards.erase(iter);
	}
}

void Game::LoadData()

{	//Characters
	//new Scarfy(this);
	//new Hazard(this);
	//new Skeleton(this, 4, Vector2{ 200,290 });
	//new Skeleton(this, 1, Vector2{ 200,400 });
	/*new Skeleton(this, 1, Vector2{ 200,400 });
	new Skeleton(this, 2, Vector2{ 280,400 });
	new Skeleton(this, 3, Vector2{ 380,400 });
	new Skeleton(this, 4, Vector2{ 480,400 });
	new Skeleton(this, 5, Vector2{ 580,400 });*/
	//Backgrounds
	Actor* temp = new Actor(this);
	temp->SetPosition(Math::Vector2(screenWidth / 2, screenHeight / 2));
	// Create the "far back" background
	BGSpriteComponent* bg = new BGSpriteComponent(temp,40);
	temp->SetScale(Math::Vector2::UnifOne);
	std::vector<TextureProp*> bgtexs = {
		GetTexture("textures/test1.png")
	};
	bg->SetBGTextures(bgtexs);
	bgtexs.clear();
	//Set Text Component
	TextComponent* textComp = new TextComponent(temp);
	textActorComp = textComp;
	textComp = nullptr;
}