#include "InputComponent.h"
#include "raylib.h"

InputComponent::InputComponent(class Actor* owner, int updateOrder)
	:Component(owner)
{
	keyInput = {};
	gamePadInput = {};
	mouseInput = {};
}

void InputComponent::AddInputKeyboard(int key)
{
	keyInput.push_back(key);
}

void InputComponent::AddInputGamePad(int key)
{
	gamePadInput.push_back(key);
}

void InputComponent::AddInputMouse(int key)
{
	mouseInput.push_back(key);
}

Vector2 InputComponent::GetMousePos()
{
	return GetMousePosition();
}

Vector2 InputComponent::GetMouseDeltaV()
{
	return GetMouseDelta();
}

void InputComponent::SetGamePad(int gamePad)
{
	gamePadId = gamePad;
}

const char* InputComponent::GetGamePadName()
{
	return GetGamepadName(gamePadId);
}

bool InputComponent::IsDown()
{
	if (!keyInput.empty())
	{
		for (int key : keyInput)
		{
			if (IsKeyDown(static_cast<KeyboardKey>(key)))
			{
				return true;
			}
		}
	}
	if (!gamePadInput.empty() && (IsGamepadAvailable(gamePadId)))
	{
		for (int key : gamePadInput)
		{
			if (IsGamepadButtonDown(gamePadId, static_cast<GamepadButton>(key)))
			{
				return true;
			}
		}
	}
	if (!mouseInput.empty())
	{
		for (int key : mouseInput)
		{
			if (IsMouseButtonDown(static_cast<MouseButton>(key)))
			{
				return true;
			}
		}
	}
	return false;
}

bool InputComponent::IsPressed()
{
	if (!keyInput.empty())
	{
		for (int key : keyInput)
		{
			if (IsKeyPressed(static_cast<KeyboardKey>(key)))
			{
				return true;
			}
		}
	}
	if (!gamePadInput.empty() && (IsGamepadAvailable(gamePadId)))
	{
		for (int key : gamePadInput)
		{
			if (IsGamepadButtonPressed(gamePadId, static_cast<GamepadButton>(key)))
			{
				return true;
			}
		}
	}
	if (!mouseInput.empty())
	{
		for (int key : mouseInput)
		{
			if (IsMouseButtonPressed(static_cast<MouseButton>(key)))
			{
				return true;
			}
		}
	}
	return false;
}

bool InputComponent::IsReleased()
{
	if (!keyInput.empty())
	{
		for (int key : keyInput)
		{
			if (IsKeyReleased(static_cast<KeyboardKey>(key)))
			{
				return true;
			}
		}
	}
	if (!gamePadInput.empty() && (IsGamepadAvailable(gamePadId)))
	{
		for (int key : gamePadInput)
		{
			if (IsGamepadButtonReleased(gamePadId, static_cast<GamepadButton>(key)))
			{
				return true;
			}
		}
	}
	if (!mouseInput.empty())
	{
		for (int key : mouseInput)
		{
			if (IsMouseButtonReleased(static_cast<MouseButton>(key)))
			{
				return true;
			}
		}
	}
	return false;
}

float InputComponent::GetAxisMovement(int movementAxis)
{
	return GetGamepadAxisMovement(gamePadId, (static_cast<GamepadAxis>(movementAxis)));
}

bool InputComponent::IsUp()
{
	if (!keyInput.empty())
	{
		for (int key : keyInput)
		{
			if (IsKeyUp(static_cast<KeyboardKey>(key)))
			{
				return true;
			}
		}
	}
	if (!gamePadInput.empty() && (IsGamepadAvailable(gamePadId)))
	{
		for (int key : gamePadInput)
		{
			if (IsGamepadButtonUp(gamePadId, static_cast<GamepadButton>(key)))
			{
				return true;
			}
		}
	}
	if (!mouseInput.empty())
	{
		for (int key : mouseInput)
		{
			if (IsMouseButtonUp(static_cast<MouseButton>(key)))
			{
				return true;
			}
		}
	}
	return false;
}