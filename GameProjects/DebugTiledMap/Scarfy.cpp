#include "Scarfy.h"
#include "SpriteComponent.h"
#include "AnimSpriteSheetComponent.h"
#include "InputComponent.h"
#include "CircleComponent.h"
#include "Game.h"
#include "Random.h"
#include "Hazard.h"

Scarfy::Scarfy(Game* game)
	:Actor(game)
{
	//Create a Sprite for class
	mAnimSpriteComp = new AnimSpriteSheetComponent(this);
	mAnimSpriteComp->SetTexture(game->GetTexture("textures/scarfy.png"));

	//Set the animations first Non Vector and vector for testing
	mAnimSpriteComp->SetAnim("Walk",true, mAnimSpriteComp->GetTexWidth()/6, mAnimSpriteComp->GetTexHeight(),6);

	//std::vector<std::tuple<int, int>> anims = { std::tuple <int,int>(0, 0), std::tuple <int,int>(1,0), std::tuple <int,int>(2,0), std::tuple <int,int>(3,0), std::tuple <int,int>(4,0), std::tuple <int,int>(5,0) };
	//animSpriteComp->SetAnim("Walk", true, animSpriteComp->GetTexWidth() / 6, animSpriteComp->GetTexHeight(), anims);
	mAnimSpriteComp->SetCurAnim("Walk", 6.0f);
	//Set Position to be middle of screen
	Math::Vector2 pos;
	pos.x = (game->GetScreenWidth() / 2); //- (animSpriteComp->GetTexWidth()) ;
	//Make Scarfy look like he is walking on bottom of screen
	pos.y = (game->GetScreenHeight()) - (mAnimSpriteComp->GetTexHeight()/2);

	SetPosition(pos);
	//Add the input Component and integrate with Move Component
	mInputMoveJump = new InputComponent(this);
	//Keyboard Input
	mInputMoveJump->AddInputKeyboard(KEY_W);
	mInputMoveJump->AddInputKeyboard(KEY_UP);
	mInputMoveJump->AddInputKeyboard(KEY_SPACE);
	mInputMoveJump->SetGamePad(0);
	mInputMoveJump->AddInputGamePad(GAMEPAD_BUTTON_RIGHT_FACE_DOWN);
	//Add Circle Component for Colision
	mCircle = new CircleComponent(this);
	mCircle->SetRadius((mAnimSpriteComp->GetTexWidth() / 4 ));

	//Snuffy is too simple to need a movecomponent
	//Set Ground plane
	//mGroundPlane = GetPosition().y >= game->GetScreenHeight() -};
	mGroundPlane = pos.y;
}

void Scarfy::UpdateActor(float deltaTime)
{
	if (checkIfGrounded())
	{
		mVelocity = 0;
		mInAir = false;
		mAnimSpriteComp->SetAnimFPS(6.0f);
	}
	else
	{
		//Still in air(if its affected by time use delta time)
		mVelocity += (mGravity * deltaTime);
		mInAir = true;
	}
	SetPosition(Math::Vector2(GetPosition().x,GetPosition().y + (mVelocity * deltaTime)));

	//Check for Collisions
	// Do we intersect with an asteroid?
	for (auto haz : GetGame()->GetHazards())
	{
		if (Intersect(*mCircle, *(haz->GetCircle())))
		{
			//TraceLog(LOG_INFO, "Collision");
			// The first asteroid we intersect with,
			// set ourselves and the asteroid to dead
			SetState(State::EDead);
			haz->SetState(State::EDead);
			break;
		}
	}
	//mCircle->DrawDebugCircle(BEIGE);
}

void Scarfy::ActorInput()
{
	
	if ((mInputMoveJump->IsPressed() || (mInputMoveJump->GetAxisMovement(GAMEPAD_AXIS_LEFT_Y) < (-0.5))) && !mInAir)
	{
		mVelocity += mJumpVelocity;
		mAnimSpriteComp->SetAnimFPS(0.0f);
	}
}

bool Scarfy::checkIfGrounded()
{
	//return data.pos.y >= windowHeight - data.rec.height;
	return ((GetPosition().y >= mGroundPlane) && (mVelocity>=0));
}
