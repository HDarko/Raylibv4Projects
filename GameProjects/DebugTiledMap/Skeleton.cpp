#include "Skeleton.h"
#include "SpriteComponent.h"
#include "AnimSpriteFilesComponent.h"
#include "Game.h"



Skeleton::Skeleton(Game* game, int state, Math::Vector2 pos)
	:Actor(game)
{
	SetPosition(pos);

	//Create a sprite Component and set Texture

	AnimSpriteFilesComponent* sc = new AnimSpriteFilesComponent(this);
	std::vector<TextureProp*> anims = {
		game->GetTexture("textures/Character01.png"),
		game->GetTexture("textures/Character02.png"),
		game->GetTexture("textures/Character03.png"),
		game->GetTexture("textures/Character04.png"),
		game->GetTexture("textures/Character05.png"),
		game->GetTexture("textures/Character06.png"),
		game->GetTexture("textures/Character07.png"),
		game->GetTexture("textures/Character08.png"),
		game->GetTexture("textures/Character09.png"),
		game->GetTexture("textures/Character10.png"),
		game->GetTexture("textures/Character11.png"),
		game->GetTexture("textures/Character12.png"),
		game->GetTexture("textures/Character13.png"),
		game->GetTexture("textures/Character14.png"),
		game->GetTexture("textures/Character15.png"),
		game->GetTexture("textures/Character16.png"),
		game->GetTexture("textures/Character17.png"),
		game->GetTexture("textures/Character18.png"),
	};
	//int x = anims[0]->width;Test Code
	//Texture2D y = (anims[0]->texture);
	sc->SetAnimTexturesFiles(anims);
	sc->SetAnim("Walk", 0, 5, true);
	sc->SetAnim("Jump", 6, 14, false);
	sc->SetAnim("Punch", 15, 17, false);
	sc->SetAnim("JumpL", 6, 14, true);
	sc->SetAnim("PunchL", 15, 17, true);
	if (state == 1) {
		sc->SetCurAnim("Punch");
		sc->SetAnimFPS(1.0f);
	}
	if (state == 2)
	{
		sc->SetCurAnim("Walk");
		sc->SetAnimFPS(3.0f);
	}
	if (state == 4)
	{
		sc->SetCurAnim("JumpL");
		sc->SetAnimFPS(5.0f);
	}
	if (state == 5)
	{
		sc->SetCurAnim("PunchL");
		sc->SetAnimFPS(3.0);
	}
	//Create a input component
	sc = nullptr;
	//AddComponent(std::move(sc));
	//Create a move Component and set a forward Speed
	//MoveComponent* mc = new MoveComponent(this);
	//mc->SetForwardSpeed(150.0f);

}


