// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "SpriteComponent.h"
#include "Actor.h"
#include "Game.h"
#include "raylib.h"
#include "MathConvertor.h"
#include "raymathMine.h"
#include "SanjayMath.h"

SpriteComponent::SpriteComponent(Actor* owner, int drawOrder)
	:Component(owner)
	, mTexture(nullptr)
	, mDrawOrder(drawOrder)
{
	mOwner->GetGame()->AddSprite(this);
	mRectOrigin = new RayMath::Vector2();
	mRectOrigin->x = 0.0f;
	mRectOrigin->y = 0.0f;
}

SpriteComponent::~SpriteComponent()
{
	if (mTexture !=nullptr)
	{
		mTexture = nullptr;
	}
	if (mRectOrigin != nullptr)
	{
		delete mRectOrigin;
		mRectOrigin = nullptr;
	}
	mOwner->GetGame()->RemoveSprite(this);
}

void SpriteComponent::Draw()
{
	if (mTexture)
	{
		// Source rectangle (part of the texture to use for drawing)
		Rectangle sourceRec = { mRectOrigin->x, mRectOrigin->y, (float)mTexture->width, (float)mTexture->height };
		Rectangle destRec;
		// Scale the width/height by owner's scale
		destRec.width = static_cast<int>(mTexture->width * mOwner->GetScale().x);
		destRec.height = static_cast<int>(mTexture->height * mOwner->GetScale().y);
		// Center the rectangle around the position of the owner
		//destRec.x = static_cast<int>(mOwner->GetPosition().x - destRec.width / 2);
		//destRec.y = static_cast<int>(mOwner->GetPosition().y - destRec.height / 2);
		destRec.x = static_cast<int>(mOwner->GetPosition().x);
		destRec.y = static_cast<int>(mOwner->GetPosition().y);
		//destRec.x = 512 / 2;
		//destRec.y = 380 / 2;
		Math::Vector2 origin;
		 //origin.Set( (float)mTexture->width, (float)mTexture->height);
		origin.Set((float)mTexture->width /2 , (float)mTexture->height/2);
		
		 Vector2 originRV = MathHelper::NameSpVec2ToVec2(MathHelper::Vec2ToRVec2(origin));
		// Draw (have to convert angle from radians to degrees)
		DrawTexturePro((mTexture->texture), sourceRec, destRec, originRV, mOwner->GetRotation() / DEG2RAD, WHITE);
	}
}

void SpriteComponent::SetTexture(TextureProp* textureProp)
{
	// Set width/height
	mTexture = textureProp;
}

int SpriteComponent::GetTexHeight() const { return mTexture->height; }
int SpriteComponent::GetTexWidth() const { return mTexture->width; }



