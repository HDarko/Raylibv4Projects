// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include "Component.h"
//Forward Declarations
class Actor;
struct Color;
struct TextureProp;
namespace RayMath {
	struct Vector2;
}

class SpriteComponent : public Component
{
public:
	// (Lower draw order corresponds with further back)
	SpriteComponent(class Actor* owner, int drawOrder = 100);
	~SpriteComponent();

	virtual void Draw();
	virtual void SetTexture(TextureProp* texture);

	int GetDrawOrder() const { return mDrawOrder; }
	int GetTexHeight() const;
	int GetTexWidth() const;
protected:
	//Contains width and height of Texture
	TextureProp* mTexture;
	int mDrawOrder;
	//Section to start the rectangle of Texture. Used in AnimSpriteSheetComponent
	RayMath::Vector2* mRectOrigin;
};


