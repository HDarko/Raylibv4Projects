#pragma once
#include "SpriteComponent.h"
#include <string>

class TextComponent :
    public SpriteComponent
{
public:
	// (Lower draw order corresponds with further back)
	TextComponent(class Actor* owner, int drawOrder = 105);
	~TextComponent();
	virtual void SetText(std::string text, int fontSize, struct Color color, int x, int y);
	void Draw() override;
private:
	std::string mText;
	struct Color* mColor;
	int mFontSize = 0;
	int posX = 0;
	int posY = 0;
};

