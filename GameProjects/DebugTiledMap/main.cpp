

/*******************************************************************************************

*
********************************************************************************************/
#include "Game.h"

int main(void)
{
    char gameTitle[] = "Dapper Dasher!";
    // Initialization
    Game game(512, 380, gameTitle, -1);
    bool sucess = game.Initialize();
    //--------------------------------------------------------------------------------------
    //SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    // Main game loop
    if (sucess)
    {
        game.RunLoop();
    }
    game.ShutDown();
    return 0;
};

