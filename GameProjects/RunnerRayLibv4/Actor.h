#pragma once
#include <vector>
#include "raylib.h"
#include "SanjayMath.h"
//Forward Declarations
class Game;
class Component;

class Actor
{
	public:
		//Used to track state of actor
		enum class State
		{
			EActive,
			EDead,
			EPaused
		};
		//Constructor/Destructor
		Actor(Game* game);
		virtual ~Actor();
		//Update function from game (Not Overridable)
		void Update(float deltaTime);
		void UpdateComponents(float deltaTime);

		//Any actor-specific update code (overridable)
		virtual void UpdateActor(float deltaTime);
		virtual void ActorInput();

		void ProcessInput();
		State GetState() const { return mState; }
		void SetState(State state) { mState = state; }

		Game* GetGame() { return mGame; }

		//Getters/setters
		const Math::Vector2& GetPosition() const { return mPosition; }
		void SetPosition(const Math::Vector2& pos) { mPosition = pos; }
		float GetScale() const { return mScale; }
		void SetScale(float scale) { mScale = scale; }
		float GetRotation() const { return mRotation; }
		//Based on Unit Cicle Rotation, 0 = Right , Math::Pi / 2 is top,
		// Math::Pi is Left, 3PI/2 is Bottom
		void SetRotation(float rotation) { mRotation = rotation; }

		Math::Vector2 GetForward() const { return Math::Vector2(Math::Cos(mRotation), -Math::Sin(mRotation)); }

		//Add/Remove Components
		void AddComponent(class Component* component);
		void RemoveComponent(class Component* component);
		//Actor's state
		State mState;

		//Transform
		Math::Vector2 mPosition; //Center Position of Actor
		float mScale; //Uniforms scale of actor (1.0f for 100%)
		float mRotation; //Rotation angle in radians

		//Components held by this actor 
		std::vector<class Component*> mComponents;
		class Game* mGame;

};

