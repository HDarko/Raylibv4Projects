#include <algorithm>
#include "AnimSpriteFilesComponent.h"
#include "Component.h"
#include "Actor.h"
#include "Game.h"


AnimSpriteFilesComponent::AnimSpriteFilesComponent(Actor* owner, int drawOrder)
	:SpriteComponent{ owner, drawOrder }, mCurrFrame{ 0.0f }, mAnimFPS{ 8.0f }, mCurAnim{ nullptr }
{

}

struct AnimwithFrames
{
	int animFrameStart{ 0 };
	int animFrameStop{ 0 };
	bool looping = false;
};

void AnimSpriteFilesComponent::SetAnimTexturesFiles(const std::vector<TextureProp*>& textures)
{
	mAnimTextures = textures;

	if (mAnimTextures.size() > 0)
	{
		mCurrFrame = 0;
		SetTexture(mAnimTextures[0]);
	}
}

void AnimSpriteFilesComponent::Update(float deltatime)
{
	//update the mCurFrame then make sure it is less
	//than num of textures (wrap back to beginiing if necessary)
	SpriteComponent::Update(deltatime);

	//Extra code to check the aimation 

	if (mAnimTextures.size() > 0 and mCurAnim != nullptr)
	{
		//update the current frame based on frame rate and delta time
		mCurrFrame += (mAnimFPS * deltatime);


		//wrap back if necessary
		if (mCurrFrame > (mCurAnim->animFrameStop) + 1)
		{
			if (mCurAnim->looping)
			{
				mCurrFrame = mCurAnim->animFrameStart;
			}
			else
			{
				mCurrFrame = mCurAnim->animFrameStop;
			}
		}

		//Set the current texture
		SetTexture(mAnimTextures[static_cast<int>(mCurrFrame)]);
	};
}
//Update animation frame (overriden from component)

void AnimSpriteFilesComponent::SetAnim(std::string nameofAnim, int animStartInText, int animEndInText, bool looping)
{

	mAnims[nameofAnim] = new AnimwithFrames{ animStartInText, animEndInText, looping };

}



AnimSpriteFilesComponent::~AnimSpriteFilesComponent()
{
	//We might delete this component and actor but game continues so only
	//Unload at game shutdown	
	for (auto& text : mAnimTextures)
	{
		text = nullptr;
	}
	for (auto& anim : mAnims)
	{
		delete anim.second;
		anim.second = nullptr;
	}
	mAnimTextures.clear();
	mAnims.clear();
	mTexture = nullptr;

}

void AnimSpriteFilesComponent::SetCurAnim(std::string nameofAnim)
{
	//std::vector<AnimSpriteComponent::AnimwithFrames*>::iterator animinVect;
	if (mAnims.find(nameofAnim) != mAnims.end())
	{
		mCurAnim = mAnims[nameofAnim];
		mCurrFrame = mCurAnim->animFrameStart;
	}
	else
	{
		//SDL_Log("Anim does not exist: %s", SDL_GetError());
		mCurAnim = nullptr;
		mCurrFrame = 0;
	}
}