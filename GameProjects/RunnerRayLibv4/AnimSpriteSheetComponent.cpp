#include <algorithm>
#include <tuple>
#include "AnimSpriteSheetComponent.h"
#include "Component.h"
#include "Actor.h"
#include "Game.h"
#include "RayMathMine.h"
struct AnimData
{
	//Frame Width and Height
	Vector2* frameWH =nullptr;
	int frameCount = 0;
	bool looping = false;
	//List of (0,1), (1,2) or positions to multiply by Rectangle width to get the
	//position of a frame to deal with horizontal/vertical layout of spritesheet
	std::vector<std::tuple<int, int>>  animFrames;
};

AnimSpriteSheetComponent::AnimSpriteSheetComponent(Actor* owner, int drawOrder)
	:SpriteComponent{ owner, drawOrder }, mCurrFrame{ 0.0f }, mAnimFPS{ 8.0f }, mCurAnim{ nullptr }
{

}

void AnimSpriteSheetComponent::Update(float deltatime)
{
	//update the mCurFrame then make sure it is less
	//than num of textures (wrap back to beginiing if necessary)
	SpriteComponent::Update(deltatime);

	//Extra code to check the aimation 

	if (mAnims.size() > 0 and mCurAnim != nullptr)
	{
		//update the current frame based on frame rate and delta time
		mCurrFrame += (mAnimFPS * deltatime);


		//wrap back if necessary
		if (mCurrFrame >= (mCurAnim->frameCount))
		{
			if (mCurAnim->looping)
			{
				mCurrFrame = 0.0f;
			}
			else
			{
				mCurrFrame = (mCurAnim->frameCount) - 1;
			}
		}

		//Set the current texture dimensions
		mTexture->width = mCurAnim->frameWH->x;
		mTexture->height = mCurAnim->frameWH->y;
		//Now we pick the current frame x and y to start drawing rect based on vector or number
		if (mCurAnim->animFrames.empty())
		{
			mRectOrigin->x = static_cast<int>(mCurrFrame) * mTexture->width;
		}
		else
		{
			mRectOrigin->x = std::get<0>(((mCurAnim->animFrames))[static_cast<int>(mCurrFrame)]) * (mTexture->width);
			mRectOrigin->y = std::get<1>(((mCurAnim->animFrames))[static_cast<int>(mCurrFrame)]) * (mTexture->height);
		}
	};
}


void AnimSpriteSheetComponent::SetAnim(std::string nameofAnim, bool doesLoop, float frameWidth, float frameHeight, std::vector<std::tuple<int, int>>& animFramePos)
{
	Vector2* spriteFrameWH = new Vector2();
	AnimData* anim = new AnimData();
	anim->frameCount = animFramePos.size();
	anim->animFrames = std::move(animFramePos);
	anim->looping = doesLoop;
	anim->frameWH = spriteFrameWH;
	spriteFrameWH->x = frameWidth;
	spriteFrameWH->y = frameHeight;
	mAnims[nameofAnim] = anim;
	anim = nullptr;
	spriteFrameWH = nullptr;
	//animFramePos = nullptr;

}

void AnimSpriteSheetComponent::SetAnim(std::string nameofAnim, bool doesLoop, float frameWidth, float frameHeight, int frameCount)
{
	Vector2* spriteFrameWH = new Vector2();
	spriteFrameWH->x = frameWidth;
	spriteFrameWH->y = frameHeight;
	std::vector<std::tuple<int, int>>  nullVector;
	mAnims[nameofAnim] = new AnimData{ spriteFrameWH, frameCount, doesLoop, std::move(nullVector)};
	spriteFrameWH = nullptr;
}



AnimSpriteSheetComponent::~AnimSpriteSheetComponent()
{
	//We might delete this component and actor but game continues so only
	//Unload at game shutdown	
	for (auto& anim : mAnims)
	{
		delete  anim.second->frameWH;
		//delete anim.second->animFrames;
		anim.second->frameWH = nullptr;
		//anim.second->animFrames = nullptr;
		anim.second->animFrames.clear();
	}
	mAnims.clear();
	mTexture = nullptr;
}

void AnimSpriteSheetComponent::SetCurAnim(std::string animName, float fPS)
{
	SetAnimFPS(fPS);

	if (mAnims.find(animName) != mAnims.end())
	{
		mCurAnim = mAnims[animName];
		mCurrFrame = 0.0f;
		//Update height and width of texture as it is a portion of image
		//Not the full image
		mTexture->width = mCurAnim->frameWH->x;
		mTexture->height = mCurAnim->frameWH->y;
	}
	else
	{
		//SDL_Log("Anim does not exist: %s", SDL_GetError());
		std::string errorMes = "Anim does not exist: " + animName;
		mCurAnim = nullptr;
		mCurrFrame = 0;
		TraceLog(LOG_ERROR, errorMes.c_str());
	}
}