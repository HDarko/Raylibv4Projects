#pragma once
#include "Component.h"
#include "SanjayMath.h"
//For Simple intersections
class CircleComponent :
    public Component
{
public:
	CircleComponent(class Actor* owner);

	void SetRadius(float radius) { mRadius = radius; }
	float GetRadius() const;
	void DrawDebugCircle(struct Color color, bool filled = false);
	//void DrawDebug(struct Color color, bool filled = false);

	const Math::Vector2& GetCenter() const;
private:
	float mRadius;
};

bool Intersect(const CircleComponent& a, const CircleComponent& b);
