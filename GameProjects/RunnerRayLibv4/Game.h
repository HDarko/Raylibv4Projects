#pragma once
#include <unordered_map>
#include <string>
//Forward declare the stuct before the typedef
struct Texture;
typedef Texture Texture2D;
struct TextureProp;

class Game
{
	public:
		Game(int screenWidth, int screenHeight, char* gameTitle, int FPS);

		bool Initialize();
		void RunLoop();
		void ShutDown();

		/*Actor Functions*/
		void AddActor(class Actor* actor);
		void RemoveActor(class Actor* actor);
		/*Sprite and Texture Functions*/
		void AddSprite(class SpriteComponent* sprite);
		void RemoveSprite(class SpriteComponent* sprite);

		TextureProp* GetTexture(const std::string& fileName);
		bool IsTextureLoaded(const std::string& fileName);

		//Screen Functions
		int GetScreenHeight() const { return screenHeight; }
		int GetScreenWidth() const { return screenWidth; }

		// Game-specific (add/remove Hazards)
		void RemoveHazard(class Hazard* haz);
		void AddHazard(class Hazard* haz);
		class TextComponent* textActorComp;
		std::vector<class Hazard*> GetHazards() { return mHazards; }
		void CheckGameState();
	private:
		//Helper functions for the game loop
		void ProcessInput();
		void UpdateGame();
		void GenerateOutput();
		void LoadData();
		void UnloadData();

		// All the actors in the game
		std::vector<class Actor*> mActors;
		// Any pending actors
		std::vector<class Actor*> mPendingActors;
		// Map of textures loaded
		std::unordered_map<std::string, Texture2D*> mTextures;
		  
		//-----------------GAME VARIABLES---------------------------
		//Screen Dimensions
		 int screenWidth;
		 int screenHeight;
		 int spriteWidth{ 50 };
		 int spriteHeight{ 50 };
		 char* gameTitle;

		 //Should Game still run
		 bool mIsRunning;
		 int mFPS;

		//FPS, Time and Physics 						
		double mMaxDeltaTime{0.05};					
		double mPhysicsClock;						//Accumated time in secs for physics simulations since game started
		double mPhysicsTimeStep{0.01};				//Fixed time step by which the physics simulation moves by 
		double mPhysicsTimeAccumulator{ 0.0 };		//Determines the number of physics simulation runs we need to
													//do in a single frame. Number of Steps = (accumulator - physicsTimeStep) till accumulator < physicsTimeStep
		double mPhysicsInterpolationFactor{0.0};

		// Track if we're updating actors right now
		bool mUpdatingActors;

		// All the sprite components drawn
		std::vector<class SpriteComponent*> mSprites;

		//Game Specific
		//Speed
		float jumpSpeed{ 120.0f };
		const float gravity{ -40.5f };
		float velocity{ 0.0f };
		//Inputs
		int mKeyPressed{0};
		bool jumpPressed{ false };
		std::vector<class Hazard*> mHazards;

};

