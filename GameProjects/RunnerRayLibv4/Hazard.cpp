#include "Hazard.h"
#include "AnimSpriteSheetComponent.h"
#include "Game.h"
#include "CircleComponent.h"
#include "SanjayMath.h"
#include "MoveComponent.h"

Hazard::Hazard(Game* game)
	:Actor(game)
{
	//Create a Sprite for class
	mAnimSpriteComp = new AnimSpriteSheetComponent(this);
	mAnimSpriteComp->SetTexture(game->GetTexture("textures/12_nebula_spritesheet.png"));

	//Set the animations first Non Vector and vector for testing
	mAnimSpriteComp->SetAnim("Idle", true, mAnimSpriteComp->GetTexWidth() / 8, mAnimSpriteComp->GetTexHeight() / 8, 8);

	//std::vector<std::tuple<int, int>> anims = { std::tuple <int,int>(0, 0), std::tuple <int,int>(1,0), std::tuple <int,int>(2,0), std::tuple <int,int>(3,0), std::tuple <int,int>(4,0), std::tuple <int,int>(5,0) };
	//animSpriteComp->SetAnim("Walk", true, animSpriteComp->GetTexWidth() / 6, animSpriteComp->GetTexHeight(), anims);
	mAnimSpriteComp->SetCurAnim("Idle", 6.0f);
	//Set Position to be just outside to the right of screen
	Math::Vector2 pos;
	pos.x = (game->GetScreenWidth()) + (mAnimSpriteComp->GetTexWidth());
	pos.y = (game->GetScreenHeight()) - (mAnimSpriteComp->GetTexHeight() / 2);
	SetPosition(pos);
	//Set Rotation, pointing to the Left
	SetRotation(Math::Pi);
	//Set Speed of Harzard with a MoveComponent
	mMoveComp = new MoveComponent(this);
	mMoveComp->SetForwardSpeed(95.0f);
	// Create a circle component (for collision)
	mCircle = new CircleComponent(this);
	mCircle->SetRadius((mAnimSpriteComp->GetTexWidth()) / 4);
	//Set Finish Line
	mFinishLine = (game->GetScreenWidth() / 2) - (2 * (mAnimSpriteComp->GetTexWidth()));
	game->AddHazard(this);
}

Hazard::~Hazard()
{
	mCircle = nullptr;
	mAnimSpriteComp = nullptr;
	mGame->RemoveHazard(this);
}

void Hazard::UpdateActor(float deltaTime)
{
	//mCircle->DrawDebugCircle(DARKPURPLE, true);
	if ((GetPosition().x) <= mFinishLine)
	{
		SetState(State::EDead);
	}
}
