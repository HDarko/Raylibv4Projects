#pragma once
#include "Actor.h"
class CircleComponent;
class Hazard :
    public Actor
{
public:
	Hazard(class Game* game);
	~Hazard();
	void UpdateActor(float deltaTime) override;

	 CircleComponent* GetCircle() { return mCircle; }
private:
	 CircleComponent* mCircle;
	 int mFinishLine;
	class AnimSpriteSheetComponent* mAnimSpriteComp;
	class MoveComponent* mMoveComp;
};

