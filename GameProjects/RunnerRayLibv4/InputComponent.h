#pragma once
#include <vector>
#include "Component.h"
#include <string>

// NOTE: Gamepad name ID depends on drivers and OS
#define XBOX360_LEGACY_NAME_ID  "Xbox Controller"
#if defined(PLATFORM_RPI)
#define XBOX360_NAME_ID     "Microsoft X-Box 360 pad"
#define PS3_NAME_ID         "PLAYSTATION(R)3 Controller"
#else
#define XBOX360_NAME_ID     "Xbox 360 Controller"
#define PS3_NAME_ID         "PLAYSTATION(R)3 Controller"
#endif
/* Axis for Gamepad
  Left Thumbstick - Axis 0
  Right Thumbstick - Axis 1
  Right - 1.00f
  Left  - -1.00f
  Up    - -1.00f
  Down  - 1.00f

*/
namespace RayMath {
	struct Vector2;
}
class InputComponent : public Component
{
	//Each instance of this class represents a single ingame action and the multiple triggers for it
	public:
		InputComponent(class Actor* owner, int updateOrder = 10);
		//Keyboard
		void AddInputKeyboard(int key);
		// Mouse
		void AddInputMouse(int key);
		struct Vector2 GetMousePos();
		//Returns a Vector2 with the difference between
		//the current and previous position of the mouse in a frame.
		//Useful for creating camera scrolling, among others.
		struct Vector2 GetMouseDeltaV();
		//GamePad
		void SetGamePad(int gamePad);
		void AddInputGamePad(int key);
		const char* GetGamePadName();

		bool IsDown();
		bool IsPressed();
		bool IsUp();
		bool IsReleased();
		float GetAxisMovement(int movementAxis);
	private:
			int  gamePadId;

		std::vector<int> keyInput;
		std::vector<int> gamePadInput;
		std::vector<int> mouseInput;
};

