#pragma once
#include "SanjayMath.h"
#include "RayMathMine.h"

namespace MathHelper
{
	RayMath::Vector2 Vec2ToRVec2(Math::Vector2 myVec)
	{
		RayMath::Vector2 dest;
		dest.x = myVec.x;
		dest.y = myVec.y;
		return dest;
	}

	Vector2 NameSpVec2ToVec2(RayMath::Vector2 nameVect)
	{
		Vector2 dest = {nameVect.x, nameVect.y};
		return dest;
	}

	Vector3 NameSpVec3ToVec3(RayMath::Vector3 nameVect)
	{
		Vector3 dest = { nameVect.x, nameVect.y, nameVect.z };
		return dest;
	}

	RayMath::Vector2 Vec2ToNameSpVec2(Vector2 nameVect)
	{
		RayMath::Vector2 dest = { nameVect.x, nameVect.y };
		return dest;
	}

	RayMath::Vector3 Vec3ToNameSpVec3(Vector3 nameVect)
	{
		RayMath::Vector3 dest = { nameVect.x, nameVect.y, nameVect.z };
		return dest;
	}

	RayMath::Vector3 Vec3ToRVec3(Math::Vector3 myVec)
	{
		RayMath::Vector3 dest;
		dest.x = myVec.x;
		dest.y = myVec.y;
		dest.z = myVec.z;
		return dest;
	}

	RayMath::Vector4 Maxt4ToRVec4(Math::Quaternion myQuart)
	{
		RayMath::Vector4 dest;
		dest.x = myQuart.x;
		dest.y = myQuart.y;
		dest.z = myQuart.z;
		dest.w = myQuart.w;
		return dest;
	}

	Matrix MaxtrixX4ToMatrix(Math::Matrix4 myMatrix)
	{
		
		Matrix dest;
		dest.m0 = myMatrix.mat[0][0];
		dest.m4 = myMatrix.mat[0][1];  // Matrix first row (4 components)
		dest.m8 = myMatrix.mat[0][2];
		dest.m12 = myMatrix.mat[0][3];

		dest.m1 = myMatrix.mat[1][0];
		dest.m5 = myMatrix.mat[1][1]; // Matrix second row (4 components)
		dest.m9 = myMatrix.mat[1][2];
		dest.m13 = myMatrix.mat[1][3];

		dest.m2 = myMatrix.mat[2][0];
		dest.m6 = myMatrix.mat[2][1];	// Matrix third row (4 components)
		dest.m10 = myMatrix.mat[2][2];
		dest.m14 = myMatrix.mat[2][3];

		dest.m3 = myMatrix.mat[3][0];
		dest.m7 = myMatrix.mat[3][1];	// Matrix fourth row (4 components)
		dest.m11 = myMatrix.mat[3][2];
		dest.m15 = myMatrix.mat[3][3];

		return dest;
	}
}
