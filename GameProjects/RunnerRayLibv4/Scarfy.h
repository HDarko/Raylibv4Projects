#pragma once
#include "Actor.h"
class Scarfy : public Actor
{
	public:
		Scarfy(class Game* game);
		void UpdateActor(float deltaTime) override;
		void ActorInput() override;
		bool checkIfGrounded();
	private:
		float mVelocity{ 0.0f };
		float mJumpVelocity{ -600.0f };
		float mGroundPlane{ 0.0f };
		float mGravity{ 700.0f };
		bool mInAir{false};
		class InputComponent* mInputMoveJump;
		class AnimSpriteSheetComponent* mAnimSpriteComp;
		class CircleComponent* mCircle;
};

