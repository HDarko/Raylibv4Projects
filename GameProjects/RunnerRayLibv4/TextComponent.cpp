#include "TextComponent.h"
#include "Actor.h"

TextComponent::TextComponent(Actor* owner, int drawOrder)
	:SpriteComponent{ owner, drawOrder }
{
	mColor = new Color();
}

TextComponent::~TextComponent()
{
	if (mColor != nullptr)
	{
		delete mColor;
		mColor = nullptr;
	}
}

void TextComponent::SetText(std::string text, int fontSize, Color color, int x, int y)
{
	mText = text;
	mFontSize = fontSize;
	*mColor = color;
	posX = x;
	posY = y;
}

void TextComponent::Draw()
{
	DrawText(mText.c_str(),posX, posY, mFontSize, *mColor);
}
